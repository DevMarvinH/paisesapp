import { Country } from './../../interfaces/pais.interface';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { PaisService } from '../../services/pais.service';

@Component({
	selector: 'app-ver-pais',
	templateUrl: './ver-pais.component.html',
	styles: [
	]
})
export class VerPaisComponent implements OnInit {

	pais!: Country;

	constructor (
		private activatedRoute: ActivatedRoute,
		private paisService: PaisService
	) {}

	ngOnInit(): void {
		//Leemos los parámetros que vienen en el url
		this.activatedRoute.params
			.pipe(
				//Este operador recibe los params y devuelve un Observable
				switchMap( ({ id }) => this.paisService.getPaisPorCodigo(id)),
				tap(console.log)
			)	
			.subscribe((pais: Country[]) => this.pais = pais[0]);
	}

}
